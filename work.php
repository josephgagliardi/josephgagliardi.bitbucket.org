        <?php
            $dir = "./media/portfolio";
            $dh = opendir($dir);
            while ($f = readdir($dh)) {
              $fullpath = $dir."/".$f;
              if ($f{0} == "." || !is_dir($fullpath)) continue;
              // echo "<a href=\"$fullpath\" target=\"_blank\">$f</a>\n";
                      $item = `<div class="isotope-item  renovations"> <img src="media/400x350/1.jpg" width="400" height="350" alt="img"> <div class="slide-desc"> <table> <tr> <td><h3>$f</h3> <div class="isotope-desc-content"> <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p> </div> <a class="btn btn-primary btn-lg " href="$fullpath" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td> </tr> </table> </div> </div>`;
                      echo $item;
                       }
            closedir($dh);
        ?>

<section  class="home-section text-center" id="work" >
    <h4 class="grabber-headline text-center">Explore our Award-Winning Legacy</h4>
      <br><br>
    <div class="container">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-9">
          <div class="text-left filter " >
            <ul id="filter" class="clearfix">
              <li class="title-action"><a href="" class="current btn" data-filter="*">All</a></li>
              <li><a href="" class="btn" data-filter=".renovations">Hotels</a></li>
              <li><a href="" class="btn" data-filter=".buildings">Hospitals</a></li>
              <li><a href="" class="btn" data-filter=".interior">Retail</a></li>
              <li><a href="" class="btn" data-filter=".plumbing">Renovation</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="isotope-frame">
      <div class="isotope-filter">

        <!-- <div class="isotope-item  renovations"> <img src="media/400x350/1.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div  class="isotope-item plumbing"><img src="media/400x350/2.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="isotope-item interior"> <img src="media/400x350/3.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div  class="isotope-item plumbing"> <img src="media/400x350/6.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="isotope-item  renovations"><img src="media/400x350/4.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="isotope-item  renovations"> <img src="media/400x350/5.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div  class="isotope-item buildings"> <img src="media/400x350/6.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="isotope-item interior"> <img src="media/400x350/7.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="isotope-item interior"> <img src="media/400x350/3.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div>
        <div  class="isotope-item buildings"> <img src="media/400x350/6.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>EXTERIOR DECORATION</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="include/modal-box-portfolio12.html" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td>
              </tr>
            </table>
          </div>
        </div> -->
      </div>
    </div>
  </section>