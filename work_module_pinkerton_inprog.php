  <section  class="home-section text-center" id="work" >
    <h4 class="grabber-headline text-center">Explore our Award-Winning Legacy</h4>
      <br><br>
    <div class="isotope-frame">
      <div class="isotope-filter">
       <?php
            $dir = "./media/portfolio";
            $dh = opendir($dir);
            while ($f = readdir($dh)) {
              $fullpath = $dir."/".$f;
              if ($f{0} == "." || !is_dir($fullpath)) continue;
              // echo "<a href=\"$fullpath\" target=\"_blank\">$f</a>\n";
                      $item = '<div class="isotope-item  renovations"> <img src="media/400x350/1.jpg" width="400" height="350" alt="img"> <div class="slide-desc"> <table> <tr> <td><h3>' . $f . '</h3> <div class="isotope-desc-content"> <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p> </div> <a class="btn btn-primary btn-lg " href="' . $fullpath . '" target="_blank" data-toggle="modal"  data-target="#myModal-id-12" >Learn More</a></td> </tr> </table> </div> </div>';
                      echo $item;
                       }
            closedir($dh);
        ?>
      
      </div>
    </div>
  </section>