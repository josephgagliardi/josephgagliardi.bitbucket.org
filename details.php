<?php include('head.php'); ?>

<body>
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"  >
<?php include('nav.php'); ?>
<main class="main-content" style="margin-top: 90px; padding-bottom: 40px;" >
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 ">
        <h2 class="light-font">Lake Buena Vista Orlando</h2>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 ">
            <h5>Desription</h5>
            <p>Mollis semper lobortis vitae phasellus turpis commodo libero vamus sed dolor donec turpis. Praesent sit amet idn non magna vel diam trum elementum. Maecenas quis nisi. Nulla eullam sit amet metus eget dolor semper laoreet. Etiam ante lectus venenatis at auctor in lobortis ac velit.</p>
            <div class="info-desc">
              <table>
                <tr>
                  <td><i class="fa fa-user"></i></td>
                  <th>Client ::</th>
                  <td>Monica Inc.</td>
                </tr>
                <tr>
                  <td><i class="fa fa-map-marker"></i></td>
                  <th>Location ::</th>
                  <td>Buena Vista FL 21029</td>
                </tr>
                <tr>
                  <td><i class="fa fa-area-chart"></i></td>
                  <th>Area ::</th>
                  <td>1,450,000 m2</td>
                </tr>
                <tr>
                  <td><i class="fa fa-table"></i></td>
                  <th>Finished On ::</th>
                  <td>February 10, 2015</td>
                </tr>
                <tr>
                  <td><i class="fa fa-btc"></i></td>
                  <th>Value ::</th>
                  <td>$653,000.00</td>
                </tr>
                <tr>
                  <td><i class="fa fa-bookmark"></i></td>
                  <th>Remarks ::</th>
                  <td>The project was completed under the budget, with the savings returned to the owner.</td>
                </tr>
              </table>
            </div>
            <p>Mollis semper lobortis vitae phasellus turpis commodo libero vamus sed dolor donec turpis. Praesent sit amet idn non magna vel diam trum elementum. Maece nas quis nisi. Nulla eullam sit amet metus eget dolor semper laoreet. Etiam ante lectus venenatis at auctor in lobortis ac velit. Proin rutrum turpis non accumsan aliquet odio magna luctus neque in adipiscing mi odio ac felis.</p>
            <h5>THE CHALLENGES</h5>
            <p>Scelerisque id tincidunt tincidunt neque. Donec ipsum libero suscipit a tristique sit amet dapibus nec nunc. Nunc mattis, leo eget molestie faucibus neque lacus com modo mauris ut ornare tortor turpis vitae enim.  Sed urna erat vehicula scelerisque gravida et scelerisque in metus. Donec turpis.</p>
            <h5>PROJECT GOALS</h5>
            <p>Mollis semper lobortis vitae phasellus turpis commodo libero vamus sed dolor donec turpis. Praesent sit amet idn non magna vel diam trum elementum. Maecenas quis nisi. Nulla eullam sit amet metus eget dolor semper laoreet. Etiam ante lectus venenatis at auctor in lobortis ac velit.</p>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 ">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 "> <a href="media/slider/towne-suites-macon.jpg)" class="magnific"> <img src="media/portfolio/TPS-SHS-Lake-Buena-Vista-Orlando/157558255.jpg" width="598" height="350" class="responsive-img"></a> </div>
            </div>
            <hr class=" separator_10">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-6 "> <a href="media/slider/towne-suites-macon.jpg)" class="magnific"> <img src="media/portfolio/TPS-SHS-Lake-Buena-Vista-Orlando/157558282.jpg"  alt="alt" class="responsive-img"> </a></div>
              <div class="col-xs-12 col-sm-12 col-md-6 "> <a href="media/280x280/1.jpg" class="magnific"> <img src="media/portfolio/TPS-SHS-Lake-Buena-Vista-Orlando/162729020.jpg"  alt="alt" class="responsive-img"></a> </div>
            </div>
            <hr class=" separator_10">
            <p>Dapibus eu erat.Nunc tempus mi eu nulla. Pellentesque ac ipsum vel massa imperdiet semper.  Donec congue pede eu lacus. Aliquam elementum orci vitae.</p>
            <ul class="arrow-list  unstyled clearfix">
              <li><a href="#"><i class="fa fa-long-arrow-right"></i> Maecen as er felis sed mollis semper lobortis vitae phasellus </a></li>
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Commodo libero Vivamus sed dolor. Quisque portitor leo as vitae </a></li>
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Tincidun rutrum urna turpis.  In lorem felis sollicitudin</a></li>
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Sed vestibulum ac dapibus eu erat. Nunc tempus mi eu nulla. </a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

</main>
 <div class="pre-footer-wrap">
    <div class="pre-footer">
      <div class="container">
        <div class="row"> <span class="btn-location-open"> Locate Us On The Map <i class="icon-arrow-down"></i></span> </div>
      </div>
    </div>
    <div class="pre-footer-content"> <a href="https://goo.gl/maps/QYXKkaJEzzH2" target="_blank"> <img src="media/map.jpg" alt="map"/> </a></div>
  </div>
<?php include('footer.php'); ?>

<!--HOME SLIDER--> 
<script src="plugins/iview/js/iview.js"></script> 

<!-- SCRIPTS --> 
<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script> 
<script src="js/waypoints.min.js"></script> 
<script src="plugins/bxslider/jquery.bxslider.min.js"></script> 
<script src="plugins/magnific/jquery.magnific-popup.js"></script> 
<script src="plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="js/classie.js"></script> 
<!--THEME--> 
<script src="js/cssua.min.js"></script> 
<script src="js/custom.js"></script>
</body>
</html>
