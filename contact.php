<?php include('head.php'); ?>

<body>
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"  >

  
  <?php include('nav.php'); ?>

  <main class="main-content" style="margin-top: 90px;" >
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 ">
          <div class="row">
            <div class="col-md-3">
              <div  data-animation="fadeInLeft" class="animated">
                <h4>Pinkerton & Laws </h4>
                <div class="table-contact">
                  <table>
                    <tbody>
                      <tr>
                        <td><i class="fa fa-map-marker"></i></td>
                        <td>165 Northchase Pkwy #100
                      Marietta, GA 30067</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-phone"></i></td>
                        <td>0800.123.9876</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-envelope"></i></td>
                        <td><a href="mailto:info@pinkerton-laws.com">info@pinkerton-laws.com</a></td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-fax"></i></td>
                        <td>1770.956.9000</td>
                      </tr>
                      <tr>
                        <td><i class="fa fa-clock-o"></i></td>
                        <td>Mon to Sat  ::  0900 - 1900</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-md-9">
              <div data-animation="fadeInRight" class="full-width-right animated ">
                <form novalidate id="contactForm" class="contactForm2" data-animation="bounceInUp" name="sentMessage animated">
                  <h4>SEND MESSAGE</h4>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="text" data-validation-required-message="Please enter your name." required id="name" placeholder="Your Name *" class="form-control">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="email" data-validation-required-message="Please enter your email address." required id="email" placeholder="Your Email *" class="form-control">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <input type="tel" data-validation-required-message="Please enter your phone number." required id="phone" placeholder="Your Phone *" class="form-control">
                            <p class="help-block text-danger"></p>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <p class="help-block text-danger"></p>
                          <div id="success"></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group form-group-text">
                        <textarea data-validation-required-message="Please enter a message." required id="message" placeholder="Your Message *" class="form-control"></textarea>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group text-right">
                        <button class="btn btn-primary ">SUBMIT </button>
                      </div>
                    </div>
                  </div>
                </form>
                
                <!--Contact form--> 
                <script src="plugins/contact/jqBootstrapValidation.js"></script> 
                <script src="plugins/contact/contact_me.js"></script> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <div class="map-box"> <a href="https://goo.gl/maps/ZDFmW" target="_blank"> <img src="media/map.jpg" alt="map"/> </a></div>
  <div class="pre-footer-wrap">
    <div class="pre-footer">
      <div class="container">
        <div class="row"> <span class="btn-location-open"> Locate Us On The Map <i class="icon-arrow-down"></i></span> </div>
      </div>
    </div>
    <div class="pre-footer-content"> <a href="https://goo.gl/maps/QYXKkaJEzzH2" target="_blank"> <img src="media/map.jpg" alt="map"/> </a></div>
  </div>
<?php include('footer.php'); ?>

<!--HOME SLIDER--> 
<script src="plugins/iview/js/iview.js"></script> 

<!-- SCRIPTS --> 
<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script> 
<script src="js/waypoints.min.js"></script> 
<script src="plugins/bxslider/jquery.bxslider.min.js"></script> 
<script src="plugins/magnific/jquery.magnific-popup.js"></script> 
<script src="plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="js/classie.js"></script> 
<!--THEME--> 
<script src="js/cssua.min.js"></script> 
<script src="js/custom.js"></script>
</body>
</html>
