  <!-- HEADER -->
  <div class="header">
    <div class="top-nav ">
    <div class="container" style="width: 100%; min-width: 100%;">
      <div class="row">
        <div class="col-md-5 col-xs-12"><a href="index.php" class="logo"> <img src="img/logo.png" width="275" height="76" alt="logo"/></a></div>
        <div class="col-md-7 col-xs-12" style="height: 0;">
          <div class="navbar yamm " >
              
            <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">

              <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
              <a href="#" class="navbar-brand">Menu</a> </div>
            <div id="navbar-collapse-1" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">

                <li class="dropdown"><a href="index.php">Home </a></li>
                <li class="dropdown"><a href="about.php">About Us </a></li>
                <li class="dropdown"><a href="tps-auburn-detail.php">Portfolio</a></li>
                <li class="dropdown"><a href="index.php">Contact Us</a></li>
                <li class="dropdown"><a href="index.php">Employee</a></li>
                <li class="dropdown"><a href="index.php">Community</a></li>
                <!-- <li class="dropdown"><a href="/about.php>About<b class="caret"></b> </a>
                  <ul role="menu" class="dropdown-menu">
                    <li> <a href="#work"> Awards</a> </li>
                    <li> <a href="#testimonials"  > Testimonials</a> </li>
                  </ul>
                </li>
                <li><a href="details.php"  > PROJECTS<b class="caret"></b> </a>
                  <ul role="menu" class="dropdown-menu">
                     <li> <a href="details.php"> Hospitality</a> </li>
                    <li> <a href="details.php"> Multifamily</a> </li>
                    <li> <a href="details.php"  > Retail</a> </li>
                  </ul>
                </li>
                 <li><a href="service.php"  > SERVICES <b class="caret"></b> </a>
                  <ul role="menu" class="dropdown-menu">
                  <li> <a href="service.php"  > Design & Build</a> </li>
                  <li> <a href="service.php"  > Project Management</a> </li>
                  <li> <a href="service.php"  > Renovation</a> </li>
                  </ul>
                </li>

                <li class=" yamm-fw"><a href="shop.php"  >Prospective Clients</a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="yamm-content">
                        <div class="row">
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <h3 class="t1-title">Hospitality</h3>
                            <ul>
                              <li><a href="service.php"><i class="fa fa-angle-right"></i>New Builds</a></li>
                              <li><a href="service.php"><i class="fa fa-angle-right"></i>Rennovations</a></li>

                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <h3 class="t1-title"> Multifamily</h3>
                            <ul>
                              <li><a href="service.php"><i class="fa fa-angle-right"></i>New Builds</a></li>
                              <li><a href="service.php"><i class="fa fa-angle-right"></i>Rennovations</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <h3 class="t1-title">Retail</h3>
                            <ul>
                              <li><a href="service.php"><i class="fa fa-angle-right"></i>New Builds</a></li>
                              <li><a href="service.php"><i class="fa fa-angle-right"></i>Rennovations</a></li>
                            </ul>
                          </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"> <a class="thumbnail" href="shop.php"><img width="270" height="270" alt="img" src="media/menu/promo.jpg"></a> </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"> <a class="thumbnail" href="shop.php"> <img width="350" height="140" alt="img" src="media/menu/pormo2.jpg"> </a> </div>
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"> <a class="thumbnail" href="shop.php"> <img width="350" height="140" alt="img" src="media/menu/pormo3.jpg"> </a> </div>
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"> <a class="thumbnail" href="shop.php"> <img width="350" height="140" alt="img" src="media/menu/pormo4.jpg"> </a> </div>
                        </div>
                      </div>
                    </li> -->
<!--                   </ul>
                </li> -->
<!--                 <li><a href="blog.php"  >News & Press <b class="caret"></b></a>
                  <ul role="menu" class="dropdown-menu">
                    <li> <a href="blog.php"> News</a> </li>
                    <li> <a href="blog.php"  > Social</a> </li>
                  </ul>
                </li> -->
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="row"></div>
    </div>
  </div>
  </div>
  <!-- MENU END --> 
  <!-- HEADER END --> 