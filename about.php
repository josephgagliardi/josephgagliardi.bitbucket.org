<?php include('head.php'); ?>

<body>
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"  >
<?php include('nav.php'); ?>

<main class="main-content" style="margin-top: 90px;" >
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="row">
          <div class="col-md-6 col-xs-12 text-left">
            <h2 class="light-font">Our Legacy</h2>
            <p class="text-left after-heading-info " ></p>
            <hr class=" separator_10">
            <div class="media no-margin"> <img src="media/slider/1.jpg"  alt="img" class="responsive-img"></div>
                <p><span>A journey of 10,000 miles begins with a single step. Pinkerton &amp; Laws took that first step in 1955 with their first construction project, a service station in Downtown Atlanta, when Arthur Laws and Jack Pinkerton founded the company. Sixty (60) years later, and in excess of 1,000 projects, Pinkerton &amp; Laws continues to operate with the same philosophy and values as captured in our mission statement:</span></p>
    <p><em><strong><span>Be the preferred provider in each of the niche markets we serve. Provide a level of product quality and service experience to our Clients such that we are not selected merely on price, but for long term value and our partnering approach.</span></strong></em></p>
    <p><em><strong><span>Achieve an annual profit such that the company can attract and retain the best employees since our business is based on its people, the company as an entity, and its individuals, can give back to both the business community that provides the opportunities for us to work, and the environments in which our employees</span></strong></em><em><span> <strong>and their families live, and the shareholders can be amply compensated for their investment risk.</strong></span></em></p>
    <p><span>Pinkerton &amp; Laws remains employee-owned in this day and age of public traded companies. This means Clients get direct principal involvement on each project and decisions are made at a local level. The value and trust that this approach builds with our Client base allows the company to negotiate the majority of their work.</span></p>
    <p><span>Pinkerton &amp; Laws treats the employees, subcontractors, vendors, and other business partners in much the same way. On all PL's construction sites, safety and quality go hand-in-hand and are administered from the top down. The principals have a "zero tolerance" attitude towards safety infractions. Pinkerton &amp; Laws strives to create an atmosphere where all workers are truly empowered to make a difference and look out for each other by taking safety to an individual level and that of their families. The result is a culture where we care for each other and want to work safely rather than being forced to.</span></p>

          </div>
          <div class="col-md-6 col-xs-12 text-left">
            <h2 class="light-font">Our Mission</h2>
            <p class="text-left after-heading-info " > </p>
            <hr class=" separator_10">
            <hr class=" separator_10">
            <p><span>Quality is typically defined by the obvious industry criteria such as complete adherence to plans and specifications and zero defects, but at Pinkerton &amp; Laws, quality is part of our culture. From the way we construct our buildings, to the way we treat our Clients, both internally and externally, quality is something one must focus on daily. In the field and on your projects, quality starts on day one. In fact cleanliness, attention to detail, and safety mindedness are all companions of a quality delivery. Internally we promote a quality culture within our ranks where each individual is held to a professional standard and encouraged to continuously improve in all areas of our operations.</span></p>
            <p><span>Through-out the project, we make quality an agenda item at every meeting. We construct mock-ups of critical assemblies, conduct pre-activity meetings when new subcontractors are introduced onto the job, and strive for zero defects at the conclusion. Pinkerton &amp; Laws may be the only construction service company with an in-house water intrusion specialist who reviews details well before they are faced in the field, maintains relationships with many of the key building envelope vendors, and frequently reviews progress during construction. We have also developed start-up and check procedures and have them in place for most common building systems.</span></p>
            <p><span>Although Pinkerton &amp; Laws specializes in a number of niche markets, we are notably the pre-eminent Hotel Builder in the Southeast and in the U.S. for our size, with over 34,000 units and 300 plus Hotels in our portfolio. We cover the Southeast with our offices in Atlanta and Orlando. We currently hold licenses in the following states: Alabama, Florida, Georgia, Louisiana, Mississippi, North Carolina, South Carolina, Tennessee, and Virginia. &nbsp;We have worked in over 18 states
            <p>&nbsp;</p>
            <!-- <ul class="category-list unstyled clearfix">
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Maecen as er felis sed mollis semper lobortis vitae phasellus </a></li>
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Commodo libero Vivamus sed dolor. Quisque portitor leo as vitae </a></li>
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Tincidun rutrum urna turpis.  In lorem felis sollicitudin</a></li>
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Sed vestibulum ac dapibus eu erat. Nunc tempus mi eu nulla. </a></li>
              <li><a href="#"><i class="fa fa-long-arrow-right"></i>Pellentesque ac ipsum vel massa imperdiet semper.</a></li>
            </ul> -->
            <hr class=" separator_10">
            <div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 ">
                <div class="media "> <img src="media/270x210/1.jpg"  class="responsive-img" width="270" height="210" alt="alt"/></div>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 ">
                <div class="media "> <img src="media/270x210/2.jpg" class="responsive-img" width="270" height="210" alt="alt"/></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr class=" separator_10">
  <div id="banner01" class="banner-full-width custom-color">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-7 text-right">
          <p>We are an Experienced &amp; Affordable Construction Company</p>
        </div>
        <div class="col-lg-5 col-md-5 text-right">
          <div class="btn-fw-banner"><a class="btn btn-lg" href="/about.php>LEARN MORE</a> <a class="btn btn-primary btn-lg " href="/contact.php">GET A QUOTE</a></div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 ">
        <div class="container">
          <div class="row">
            <div class="col-md-offset-3 col-md-6">
              <header class="section-header animated  animation-done fadeInUp" data-animation="fadeInUp">
                <div class="heading-wrap">
                  <h2 class="heading"><span>Team Members</span></h2>
                </div>
              </header>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <p class="text-center animated after-heading-info  animation-done fadeInUp" data-animation="fadeInUp"> Sed tempus tempor orc mauris viverra nunc diam</p>
            </div>
            <div class="col-md-3"></div>
          </div>
        </div>
        <hr class=" separator_10">
        <ul class="carousel-team bxslider" 
    data-max-slides="4" 
     data-min-slides="2" 
    data-width-slides="260" 
    data-margin-slides="15" 
        data-auto-slides="false" 
    data-move-slides="1"   
    data-infinite-slides="false" >
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/3.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">JACK THOMAS</h3>
              <h5 class="team-position">President</h5>
            </div>
          </li>
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/4.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">HARRY OLIVER</h3>
              <h5 class="team-position">Senior VP</h5>
            </div>
          </li>
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/5.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">GEORGE ADAM</h3>
              <h5 class="team-position">project supervisor</h5>
            </div>
          </li>
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/6.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">MICHAEL LUCAS</h3>
              <h5 class="team-position">senior worker</h5>
            </div>
          </li>
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/3.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">JACK THOMAS</h3>
              <h5 class="team-position">President</h5>
            </div>
          </li>
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/4.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">HARRY OLIVER</h3>
              <h5 class="team-position">Senior VP</h5>
            </div>
          </li>
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/5.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">GEORGE ADAM</h3>
              <h5 class="team-position">project supervisor</h5>
            </div>
          </li>
          <li>
            <div class="media"> <a href="#"><img src="media/270x210/6.jpg" width="268" height="220" alt="team"/></a>
              <div class="overlay-team">
                <p>&nbsp; </p>
                <p>Lorem ipsum dolor sit amet cons
                  tetur adipisicing elit sed do eiusmod tempor incididunt labore
                  et dolore sed ipsum duir magna </p>
                <p>Mob  ::  +1 (123) 456700</p>
              </div>
            </div>
            <div class="carousel-item-content ">
              <h3 class="team-name">MICHAEL LUCAS</h3>
              <h5 class="team-position">senior worker</h5>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  </div>
</main>
<div class="pre-footer-wrap">
    <div class="pre-footer">
      <div class="container">
        <div class="row"> <span class="btn-location-open"> Locate Us On The Map <i class="icon-arrow-down"></i></span> </div>
      </div>
    </div>
    <div class="pre-footer-content"> <a href="https://goo.gl/maps/QYXKkaJEzzH2" target="_blank"> <img src="media/map.jpg" alt="map"/> </a></div>
  </div>
<?php include('footer.php'); ?>

<!--HOME SLIDER--> 
<script src="plugins/iview/js/iview.js"></script> 

<!-- SCRIPTS --> 
<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script> 
<script src="js/waypoints.min.js"></script> 
<script src="plugins/bxslider/jquery.bxslider.min.js"></script> 
<script src="plugins/magnific/jquery.magnific-popup.js"></script> 
<script src="plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="js/classie.js"></script> 
<!--THEME--> 
<script src="js/cssua.min.js"></script> 
<script src="js/custom.js"></script>
</body>
</html>
