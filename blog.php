<?php include('head.php'); ?>

<body>
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"  >
<?php include('nav.php'); ?>
  <main class="main-content" style="margin-top: 90px;" >
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9 ">
          <article class="post media-image   format-image animated" data-animation="bounceInUp">
            <div class="entry-media">
              <div class="entry-thumbnail"> <a   href="post.html" ><img src="media/860x350/1.jpg" width="860" height="350" alt="img"/></a> </div>
            </div>
            <div class="entry-main">
              <div class="box-date-post"> <span class="date-1">16 </span> <span class="date-2"> JULY</span> </div>
              <div class="entry-meta clearfix">
                <ul class="unstyled clearfix">
                  <li> <a title="/" href="/">By Admin</a> </li>
                  <li>/</li>
                  <li> Home Renovation, Tips & Tricks</li>
                  <li>/</li>
                  <li> <a  href="/"> Comments: 5 </a> </li>
                </ul>
              </div>
              <h3 class="entry-title"> <a href="post.html" >Lorem ipsum dolor sit amet consectetur adipisicing </a> </h3>
              <div class="entry-content">
                <p>Mollis semper lobortis vitae phasellus turpis commodo libero vamus sed dolor donec turpis. Praesent sit amet non magna vel diam trum elementum. Maecenas quis nisi. Nulla eullam sit amet metus eget dolor semper lao ret. Lorem ipsum dolor sit amet consect adipisicing elit sed do eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad minim veniam qus ostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute.
                  irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <div class="entry-footer"> <a href="post.html" class="view-post-btn">READ MORE</a> </div>
              </div>
            </div>
          </article>
          <article class="post media-image   format-image animated" data-animation="bounceInUp">
            <div class="entry-media">
              <div class="entry-thumbnail">
                <div class="outside">
                  <p><span id="slider-prev"></span> <span id="slider-next"></span></p>
                </div>
                <ul class="carousel-post">
                  <li> <a   href="media/860x350/2.jpg"><img src="media/860x350/2.jpg" width="860" height="350" alt="img"/></a> </li>
                  <li> <a   href="media/860x350/3.jpg"><img src="media/860x350/3.jpg" width="860" height="350" alt="img"/></a> </li>
                </ul>
              </div>
            </div>
            <div class="entry-main">
              <div class="box-date-post"> <span class="date-1">16 </span> <span class="date-2"> JULY</span> </div>
              <div class="entry-meta clearfix">
                <ul class="unstyled clearfix">
                  <li> <a title="/" href="/">By Admin</a> </li>
                  <li>/</li>
                  <li> Home Renovation, Tips & Tricks</li>
                  <li>/</li>
                  <li> <a  href="/"> Comments: 5 </a> </li>
                </ul>
              </div>
              <h3 class="entry-title"> <a href="post.html" >Lorem ipsum dolor sit amet consectetur adipisicing </a> </h3>
              <div class="entry-content">
                <p>Mollis semper lobortis vitae phasellus turpis commodo libero vamus sed dolor donec turpis. Praesent sit amet non magna vel diam trum elementum. Maecenas quis nisi. Nulla eullam sit amet metus eget dolor semper lao ret. Lorem ipsum dolor sit amet consect adipisicing elit sed do eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad minim veniam qus ostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute.
                  irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <div class="entry-footer"> <a href="post.html" class="view-post-btn">READ MORE</a> </div>
              </div>
            </div>
          </article>
          <article class="post media-image   format-blockquote animated" data-animation="bounceInUp">
            <div class="entry-media">
              <div class="entry-thumbnail">
                <div class="blockquote">
                  <p>Fusce ut odio get eleifend tincidunt vestibulum ring al rsus in metus. Sep interd umli uam scelerisque. Nam odiodui vestibulum amolestie pulvinar Suspendisse nulla. Sed felis. Sed ornare eleifend tellus. Maecenas mattis massa  Nullam justo massa, adipiscing a convallis ultricies luctus et dolor. Ut  Integer et urna. Ut conse quat tincidunt tortor. Pellentesque congue semper felis. Nullam odio justo pharetra et sagittis adsc dignissim nec metus class.</p>
                  <span class="blockquote-autor">JOHN ALEXANDER</span> </div>
              </div>
            </div>
            <div class="entry-main">
              <div class="box-date-post"> <span class="date-1">16 </span> <span class="date-2"> JULY</span> </div>
              <div class="entry-meta clearfix">
                <ul class="unstyled clearfix">
                  <li> <a title="/" href="/">By Admin</a> </li>
                  <li>/</li>
                  <li> Home Renovation, Tips & Tricks</li>
                  <li>/</li>
                  <li> <a  href="/"> Comments: 5 </a> </li>
                </ul>
              </div>
              <div class="entry-footer"> <a class="view-post-btn" href="post.html">READ MORE</a> </div>
            </div>
          </article>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3"    >
          <aside class="sidebar"> 
            
            <!-- SEARCH WIDGET -->
            
            <div class="widget widget-search">
              <h3 class="widget-title">Search blog</h3>
              <div class="block_content">
                <form method="get" id="search-global-form">
                  <input type="text" placeholder="Search" name="s" id="search2" value="">
                  <button type="submit"><i class="fa fa-search"></i></button>
                </form>
              </div>
            </div>
            
            <!-- ARCHIVE WIDGET -->
            
            <div class="widget widget-category">
              <h3 class="widget-title">categories</h3>
              <div class="block_content">
                <ul class="category-list unstyled clearfix">
                  <li><a href="#">Home Renovation<span class="amount-cat">45</span></a></li>
                  <li><a href="#">Green Buildings <span class="amount-cat">17</span> </a></li>
                  <li><a href="#">Construction <span class="amount-cat">36</span> </a></li>
                  <li><a href="#">Mining Industry <span class="amount-cat">45</span> </a></li>
                  <li><a href="#">Latest Offers <span class="amount-cat">19</span> </a></li>
                </ul>
              </div>
            </div>
            
            <!-- SEARCH WIDGET -->
            
            <div class="widget widget-post">
              <h3 class="widget-title">RECENT POSTS</h3>
              <div class="block_content">
                <ul class="product-mini-list  unstyled ">
                  <li>
                    <div class="entry-thumbnail"> <a class="img" href="#"> <img src="media/60x60/3.jpg" width="60" height="60" alt="alt"/></a> </div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h5><a href="#">Eastwood Project: House Tour </a></h5>
                      </div>
                      <div class="entry-meta">
                        <div class="meta">10 FEB 2017</div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="entry-thumbnail"> <a class="img" href="#"> <img src="media/60x60/4.jpg" width="60" height="60" alt="alt"/></a> </div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h5><a href="#">Living In The Light Houses</a></h5>
                      </div>
                      <div class="entry-meta">
                        <div class="meta">11 FEB 2017</div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="entry-thumbnail"> <a class="img" href="#"> <img src="media/60x60/5.jpg" width="60" height="60" alt="alt"/></a> </div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h5><a href="#">Inside a Beautiful Brooklyn Loft </a></h5>
                      </div>
                      <div class="entry-meta">
                        <div class="meta">12 FEB 2017</div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="entry-thumbnail"> <a class="img" href="#"> <img src="media/60x60/6.jpg" width="60" height="60" alt="alt"/></a> </div>
                    <div class="entry-main">
                      <div class="entry-header">
                        <h5><a href="#">I dream of Spring. I dream of Green </a></h5>
                      </div>
                      <div class="entry-meta">
                        <div class="meta">18 FEB 2017</div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-archive">
              <h3 class="widget-title"><span><i class="fa flaticon-wrench44"></i>BLOG ARCHIVE</span></h3>
              <div class="block_content">
                <ul class="category-list unstyled clearfix">
                  <li><a href="#">January 2017</a></li>
                  <li><a href="#">December 2014 </a></li>
                  <li><a href="#">November 2014 </a></li>
                  <li><a href="#">October 2014</a></li>
                  <li class="li-last"><a href="#">September 2014 </a></li>
                </ul>
              </div>
            </div>
            <div class="widget widget-archive">
              <h3 class="widget-title">TAG CLOUD</h3>
              <div class="block_content">
                <div class="tagcloud">
                  <ul class="wp-tag-cloud">
                    <li><a    href="/">Latest Offers</a></li>
                    <li><a   href="/">Construction</a></li>
                    <li><a   href="/"> Green Buildings</a></li>
                    <li><a   href="/"> How to Build</a></li>
                    <li><a   href="/"> Room Decoration</a></li>
                    <li><a   href="/"> Tips</a></li>
                    <li><a   href="/"> Basic Tricks</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </main>
  <div class="pre-footer-wrap">
    <div class="pre-footer">
      <div class="container">
        <div class="row"> <span class="btn-location-open"> Locate Us On The Map <i class="icon-arrow-down"></i></span> </div>
      </div>
    </div>
    <div class="pre-footer-content"> <a href="https://goo.gl/maps/QYXKkaJEzzH2" target="_blank"> <img src="media/map.jpg" alt="map"/> </a></div>
  </div>
<?php include('footer.php'); ?>
<!--HOME SLIDER--> 
<script src="plugins/iview/js/iview.js"></script> 

<!-- SCRIPTS --> 
<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script> 
<script src="js/waypoints.min.js"></script> 
<script src="plugins/bxslider/jquery.bxslider.min.js"></script> 
<script src="plugins/magnific/jquery.magnific-popup.js"></script> 
<script src="plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="js/classie.js"></script> 
<!--THEME--> 
<script src="js/cssua.min.js"></script> 
<script src="js/custom.js"></script>
</body>
</html>
