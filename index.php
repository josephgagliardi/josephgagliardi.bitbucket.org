<?php include('head.php'); ?>

<body>
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"  >
  <div id="iview">
    
    <div data-iview:thumbnail="media/slider/1.jpg" data-iview:image="media/slider/1.jpg" data-iview:transition="fade"  > <video class="video" width="100%" height="100%" style="min-height: 1200px;" autoplay>
        <source src="/media/slider/Pinkerton_WebBanner.mp4" type="video/mp4">
        <source src="movie.ogg" type="video/ogg">
      Your browser does not support the video tag.
      </video>
    </div>
  </div>
<?php include('nav.php'); ?>
  <div class="banner-full-width text-center" id="banner01">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p class="brand-text" style="font-size: 48px; content-align: center;">Leading the Nation in Southern Hospitality since 1955.</p></div>
        </div>
      </div>
    </div>
  </div>
  <section class="home-section">
    <section class="carousel carousel1 animated " data-animation="fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-3 col-md-6">
            <header data-animation="fadeInUp" class="section-header animated  animation-done fadeInUp">
              <div class="heading-wrap">
                <h2 class="heading brand-text"><span>Pinkerton & Laws</span></h2>
              </div>
            </header>
          </div>
        </div>
      </div>
      <div class="container brand-text">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 aligncenter company-overview">
            <p class="pinkerton-intro" style="font-size: 24px;">Pinkerton & Laws is among the largest select service hospitality builders in the country and nationally recognized as a leader in "multi-unit" construction; yet we maintain a well-balanced and diverse portfolio consisting of multifamily, retail, senior living, assisted living, renovations of both hospitality and retail, along with many other commercial projects. With offices in Atlanta and Orlando, Pinkerton & Laws brings a diverse scope of services to our Clients that are unmatched in the industry.</p>
          </div>
          <div class="col-md-2"></div>
        </div>
      </div>
    </section>
  </section>
  <section id="charstart" class="no-bg-color-parallax parallax-red home-section no-margin animated " data-animation="fadeInUp">
    <ul class="bg-slideshow">
      <li>
        <div style="background-image:url(media/portfolio/Courtyard-Marriott-F.Walton-Beach/Courtyard-by-Marriott---Okaloosa-Island---Fort-Walton-Beach,-FL-78.jpg)" class="bg-slide"></div>
      </li>
      <li>
        <div style="background-image:url(media/portfolio/TPS-SHS-Lake-Buena-Vista-Orlando/157558255.jpg)" class="bg-slide"></div>
      </li>
    </ul>
    <div class="container">
      <div class="row">
        <div class="col-lg-12"> 
          <script src="plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 statbox">
            <div class="featured-item-simple-icon animated text-center"  data-animation="fadeInUp">
              <div class="ft-icons-simple"><i class="fa flaticon-building104"></i> </div>
              <div class="ft-content"> <span class="chart" data-percent="1000"> <span class="percent"></span> </span>
                <h6>PROJECTS DONE</h6>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 statbox">
            <div class="featured-item-simple-icon animated text-center"  data-animation="fadeInUp">
              <div class="ft-icons-simple"><i class="fa flaticon-architecture1"></i> </div>
              <div class="ft-content"> <span class="chart" data-percent="34000"> <span class="percent"></span> </span>
                <h6>UNITS BUILT</h6>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 statbox">
            <div class="featured-item-simple-icon animated text-center"  data-animation="fadeInUp">
              <div class="ft-icons-simple"><i class="fa flaticon-construction11"></i> </div>
              <div class="ft-content"> <span class="chart" data-percent="300"> <span class="percent"></span> </span>
                <h6>HOTEL PORTFOLIO</h6>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 statbox">
            <div class="featured-item-simple-icon animated text-center"  data-animation="fadeInUp">
              <div class="ft-icons-simple"><i class="fa flaticon-constructor2"></i> </div>
              <div class="ft-content"> <span class="chart" data-percent="18"> <span class="percent"></span> </span>
                <h6>ACTIVE STATES</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<section  class="home-section text-center" id="work" >
    <h4 class="grabber-headline text-center">Explore our Award-Winning Legacy</h4>
    
    <div class="isotope-frame">
      <div class="isotope-filter">
        <div class="isotope-item  renovations"> <img src="media/menu/Hotel3.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>Buena Vista</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="buena-vista-details.php" >Learn More</a></td>                  
              </tr>
            </table>
          </div>
        </div> 
        <div class="isotope-item  renovations"> <img src="media/menu/Hotel2.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>Macon Georgia</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="macon-ga-details.php" >Learn More</a></td>                  
              </tr>
            </table>
          </div>
        </div>  
        <div class="isotope-item  renovations"> <img src="media/menu/Hotel1.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>Auburn Georgia</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="tps-auburn-details.php" >Learn More</a></td>                  
              </tr>
            </table>
          </div>
        </div>
        <div class="isotope-item  renovations"> <img src="media/menu/Hotel4.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>Buena Vista</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="buena-vista-details.php" >Learn More</a></td>                  
              </tr>
            </table>
          </div>
        </div> 
        <div class="isotope-item  renovations"> <img src="media/menu/Hotel5.jpg" width="400" height="350" alt="img">
          <div class="slide-desc">
            <table>
              <tr>
                <td><h3>Buena Vista</h3>
                  <div class="isotope-desc-content">
                    <p>Levir meus, priusquam oppugnarent tempus quis, admonere dicitur. Credo quod idem mihi praesidium. </p>
                  </div>
                  <a class="btn btn-primary btn-lg " href="buena-vista-details.php" >Learn More</a></td>                  
              </tr>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </section>
  <section id="testimonials" class="home-section animated " style="background-color:#f4f4f4;"  data-animation="fadeInUp" >
    <div class="container">
      <div class="row">
        <div class="col-md-offset-3 col-md-6">
          <header class="section-header animated  animation-done fadeInUp" data-animation="fadeInUp">
            <div class="heading-wrap">
              <h2 class="heading"><span>What Our Clients Are Saying</span></h2>
            </div>
          </header>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <p class="text-center animated after-heading-info  animation-done fadeInUp" data-animation="fadeInUp"> Our Clients know the Two Names and One Firm they can Trust!</p>
        </div>
        <div class="col-md-3"></div>
      </div>
    </div>
    <ul class="carousel-4 bxslider" 
    data-max-slides="2" 
    data-width-slides="560" 
    data-margin-slides="30" 
    data-auto-slides="false" 
    data-move-slides="2"   
    data-infinite-slides="true" >
      <li>
        <div class="testi-box ">
          <div class="person-text  top ">
            <div class="arrow"></div>
            <i class="icomoon-quote-left"></i>
            <div class="testi-title">
              <h4>Trustd</h4>
            </div>
            <p>I have been using Pinkerton & Laws for over 20 years and they are always, always great!</p>
          </div>
          <div class="person-info">
            <div class="person-avatar"> <img src="media/50x50/1.jpg"  alt="alt"/> </div>
            <div class="person-name">
              <h5>Michael Bou-Sliman</h5>
              <p>Naples Hotel Group</p>
            </div>
            <div class="product-rating"> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star-o"></i></div>
          </div>
        </div>
      </li>
      <li>
        <div class="testi-box ">
          <div class="person-text  top ">
            <div class="arrow"></div>
            <i class="icomoon-quote-left"></i>
            <div class="testi-title">
              <h4>Quality Leadership</h4>
            </div>
            <p>The leadership, execution and partnership with Pinkerton & Laws has been instrumental to our success.</p>
          </div>
          <div class="person-info">
            <div class="person-avatar"> <img src="media/50x50/1.jpg"  alt="alt"/> </div>
            <div class="person-name">
              <h5>Bill Anderson</h5>
              <p>Alliance Community</p>
            </div>
            <div class="product-rating"> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star-o"></i></div>
          </div>
        </div>
      </li>
      <li>
        <div class="testi-box ">
          <div class="person-text  top ">
            <div class="arrow"></div>
            <i class="icomoon-quote-left"></i>
            <div class="testi-title">
              <h4>Better Than The Best !!!</h4>
            </div>
            <p>Pharetra libero non facilisis imperdiet mi augue feugiat nisl sit amet mollis enim velit  Vestibulum fringilla nulla ultricies sem. Maecenas ultrices faucibus felis. </p>
            <p>Maecenas tincidunt. Proin porttitor lacus eget mi. Aenean at mi. Mauris vulputate mi vitae lobortis. Pellentesque ac ipsum vel massa imperdiet semper. In maurisd libero interdum quis molestie.</p>
          </div>
          <div class="person-info">
            <div class="person-avatar"> <img src="media/50x50/1.jpg"  alt="alt"/> </div>
            <div class="person-name">
              <h5>Thomas Ben</h5>
              <p>Happy Client</p>
            </div>
            <div class="product-rating"> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa  fa-star gold"></i></div>
          </div>
        </div>
      </li>
      <li>
        <div class="testi-box ">
          <div class="person-text  top ">
            <div class="arrow"></div>
            <i class="icomoon-quote-left"></i>
            <div class="testi-title">
              <h4>Better Than The Best !!!</h4>
            </div>
            <p>Pharetra libero non facilisis imperdiet mi augue feugiat nisl sit amet mollis enim velit  Vestibulum fringilla nulla ultricies sem. Maecenas ultrices faucibus felis. </p>
            <p>Maecenas tincidunt. Proin porttitor lacus eget mi. Aenean at mi. Mauris vulputate mi vitae lobortis. Pellentesque ac ipsum vel massa imperdiet semper. In maurisd libero interdum quis molestie.</p>
          </div>
          <div class="person-info">
            <div class="person-avatar"> <img src="media/50x50/1.jpg"  alt="alt"/> </div>
            <div class="person-name">
              <h5>Thomas Ben</h5>
              <p>Happy Client</p>
            </div>
            <div class="product-rating"> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star-o"></i></div>
          </div>
        </div>
      </li>
    </ul>
  </section>
  <section class="home-section">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-3 col-md-6">
          <header class="section-header animated  animation-done fadeInUp" data-animation="fadeInUp">
            <div class="heading-wrap">
              <h2 class="heading"><span>Our Roots Grow Deep</span></h2>
            </div>
          </header>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <p data-animation="fadeInUp" class="text-center animated after-heading-info  animation-done fadeInUp">We don't just build the Southeast; we partner to lead it!</p>
        </div>
        <div class="col-md-3"></div>
      </div>
    </div>
    <section class="carousel-brand animated " data-animation="fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-md-12 ">
            <div class="text-center">
              <div class="logo-box"> <a href="#"><img src="media/brand/hh.jpg" class="brand" alt="brand"> </a></div>
              <div class="logo-box"> <a href="#"><img src="media/brand/aha.jpg" class="brand" alt="brand"> </a></div>
              <div class="logo-box"> <a href="#"><img src="media/brand/stjudes.gif" class="brand" alt="brand"> </a> </div>
              <div class="logo-box"> <a href="#"><img src="media/brand/chatl.gif" class="brand" alt="brand"> </a> </div>
              <div class="logo-box"> <a href="#"><img src="media/brand/chouse.jpg" class="brand" alt="brand"> </a> </div>
              <div class="logo-box"><a href="#"> <img src="media/brand/ada.jpg" class="brand" alt="brand"> </a> </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>
  <div class="pre-footer-wrap">
    <div class="pre-footer">
      <div class="container">
        <div class="row"> <span class="btn-location-open"> Locate Us On The Map <i class="icon-arrow-down"></i></span> </div>
      </div>
    </div>
    <div class="pre-footer-content"> <a href="https://goo.gl/maps/QYXKkaJEzzH2" target="_blank"> <img src="media/map.jpg" alt="map"/> </a></div>
  </div>
<?php include('footer.php'); ?>

<!--HOME SLIDER--> 
<script src="plugins/iview/js/iview.js"></script> 

<!-- SCRIPTS --> 
<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script> 
<script src="js/waypoints.min.js"></script> 
<script src="plugins/bxslider/jquery.bxslider.min.js"></script> 
<script src="plugins/magnific/jquery.magnific-popup.js"></script> 
<script src="plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="js/classie.js"></script> 
<!--THEME--> 
<script src="js/cssua.min.js"></script> 
<script src="js/custom.js"></script>
</body>
</html>
