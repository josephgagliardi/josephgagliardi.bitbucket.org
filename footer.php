  <footer class="footer footer-shop">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="fot-box">
            <h3 class="fot-logo"><img src="img/logo-inverted.png"  alt="logo"></h3>
            <p>Leading the Industry for more than 60 years through superior quality, unsurpassed expertise, and outstanding service. </p>
            <p>The Leading General Contractor in the Southeast</p>
          </div>
          <div class="row">
            <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
              <div class="social-box">
                <ul class="social-links">
                  <li><a target="_blank" href="https://www.facebook.com/"><i class="icomoon-facebook"></i></a></li>
                  <li class=""><a target="_blank" href="https://twitter.com/"><i class="icomoon-twitter"></i></a></li>
                  <li><a target="_blank" href="https://www.google.com/"><i class="icomoon-googleplus"></i></a></li>
                  <li class="li-last"><a target="_blank" href="https://www.pinterest.com/"><i class="icomoon-pinterest"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="fot-box">
            <h3 class="fot-title">COMPANY PAGES</h3>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <ul>
                  <li> <a href="#"> Home</a></li>
                  <li> <a href="/about.php> About</a></li>
                  <li> <a href="/details.php"> Projects</a></li>
                  <li> <a href="/service.php"> Our Services</a></li>
                  <li> <a href="/contact.php"> Contact Us</a></li>
                </ul>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <ul>
                  <li> <a href="/contact.php"> Prospective Clients</a></li>
                  <li> <a href="#testimonials">Testimonials</a></li>
                  <li> <a href="/blog.php">News & Press</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="fot-box">
            <h3 class="fot-title">Contact Us</h3>
            <div class="fot-contact">
              <div class="media-body">
                <table>
                  <tr>
                    <td><i class="fa fa-map-marker"></i></td>
                    <td>1165 Northchase Pkwy #100
                      Marietta, GA 30067</td>
                  </tr>
                  <tr>
                    <td><i class="fa fa-phone"></i></td>
                    <td>770.956.9000</td>
                  </tr>
                  <tr>
                    <td><i class="fa fa-envelope"></i></td>
                    <td><a href="mailto:contact.us@pinkerton-laws.com">contact.us@pinkerton-laws.com</a></td>
                  </tr>
                  <tr>
                    <td><i class="fa fa-fax"></i></td>
                    <td>770.618.8688</td>
                  </tr>
                  <tr>
                    <td><i class="fa fa-clock-o"></i></td>
                    <td>New Project Interest:<br>
                      <a href="mailto:jjernigan@pinkerton-laws.com">jjernigan@pinkerton-laws.com</a></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="fot-box">
            <h3 class="fot-title">PROJECTS</h3>
            <!-- Flickr--> 
            <script src="plugins/jflickrfeed/jflickrfeed.min.js" ></script> 
            <script type="text/javascript">



$(document).ready(function($) {

    
    /////////////////////////////////////
    // Flickr Feed
    /////////////////////////////////////



    // Get your flickr ID from: https://idgettr.com/



    var flickr_id = '7992704@N05';

    var $flcr_feed

    $flcr_feed = $('#flickr-feed');
    if ($flcr_feed.length) {
        $('#flickr-feed').jflickrfeed({
            limit: 9,
            qstrings: {
                id: '7992704@N05'
            },
            itemTemplate: '<li><a href="{{image_b}}" rel="prettyPhoto[flickr]"><img src="{{image_s}}" alt="{{title}}" /><span><i class="icomoon-search"></i></span></a></li>',
            itemCallback: function() {
                $("a[rel='prettyPhoto[flickr]']").prettyPhoto({
                    changepicturecallback: function() {
                        $('.pp_pic_holder').show();
                    }
                });
            }
        });
    }



    var flickr_id = '120066133@N02';

    var $flcr_feed

    $flcr_feed = $('#flickr-feed2');
    if ($flcr_feed.length) {
        $('#flickr-feed2').jflickrfeed({
            limit: 9,
            qstrings: {
                id: '120066133@N02'
            },
            itemTemplate: '<li><a href="{{image_b}}" rel="prettyPhoto[flickr]"><img src="{{image_s}}" alt="{{title}}" /><span><i class="icomoon-search"></i></span></a></li>',
            itemCallback: function() {
                $("a[rel='prettyPhoto[flickr]']").prettyPhoto({
                    changepicturecallback: function() {
                        $('.pp_pic_holder').show();
                    }
                });
            }
        });
    }

 });



</script>
            <div class="fot-contact">
              <div class="media-body">
                <ul id="flickr-feed" class="flickr-feed">
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <div class="footer-absolute">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="copy aligncenter">
            <p><a href="#" target="_blank">Pinkerton & Laws</a> &nbsp;  © 2019 All rights reserved. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<a class="scroll-top "> <i class="fa fa-angle-up"> </i></a> 