<?php include('head.php'); ?>

<body>
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"  >
  <?php include('nav.php'); ?>
  <main class="main-content" style="margin-top: 90px;">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3"    >
        <aside class="sidebar animated " data-animation="fadeInLeft" > 
          
          <!-- ARCHIVE WIDGET -->
          
          <div class="widget widget-category cms-category-list">
            <div class="block_content">
              <ul class="category-list unstyled clearfix">
                <li><a href="#"><i class="fa fa-long-arrow-right"></i>All  Standards</a></li>
                <li><a href="#"><i class="fa fa-long-arrow-right"></i>Water Intrusion Protection</a></li>
                <li><a href="#"><i class="fa fa-long-arrow-right"></i>Compliance</a></li>
                <li><a href="#"><i class="fa fa-long-arrow-right"></i>Renovation</a></li>
                <li><a href="#"><i class="fa fa-long-arrow-right"></i>MultiFamily</a></li>
                <li><a href="#"><i class="fa fa-long-arrow-right"></i>Retail Space</a></li>
              </ul>
            </div>
          </div>
          
          <!-- ARCHIVE WIDGET -->
          
          <div class="widget widget-products">
            <h3 class="widget-title">FEATURED PROJECT</h3>
            <div class="block_content"> <img src="media/268x200/1.jpg" alt="img" >
              <p>&nbsp; </p>
              <p> Phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincid
                un rutrum urna turpis.</p>
              <a href="#" class="btn-link">VIEW DETAILS</a> <a href="#" class="btn-download"><i class="fa fa-file-pdf-o"></i>DOWNLOAD CASE STUDY</a> <a href="#" class="btn-download" ><i class="fa fa-file-pdf-o"></i>DOWNLOAD BROCHURE </a> </div>
          </div>
          
          <!-- SEARCH WIDGET --> 
          
        </aside>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-9 ">
        <div class="row">
          <div class="col-md-offset-2 col-md-8">
            <header class="section-header animated  animation-done fadeInUp" data-animation="fadeInUp">
              <div class="heading-wrap">
                <h2 class="heading"><span>We Follow New Quality Standards</span></h2>
              </div>
            </header>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <p class="text-center animated after-heading-info  animation-done fadeInUp" data-animation="fadeInUp"> Our investment in Excellence Sets us Ahead</p>
          </div>
          <div class="col-md-3"></div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12  col-md-4 ">
            <div class="box-1"> <a class="box-1-title" href="#">WATER INTRUSION MANAGEMENT </a>
              <div class="media"> <a href="#"><img width="369" height="219" alt="alt" src="media/370x300/1.jpg"></a></div>
              <div class="box-1-content">
                <div class="box-1-text">
                  <p> Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                </div>
                <a class="btn btn-lg btn-read-more " href="/">READ MORE</a> </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12  col-md-4 ">
            <div class="box-1"> <a class="box-1-title" href="#">GENERAL CONTRACT</a>
              <div class="media"> <a href="#"><img width="369" height="219" alt="alt" src="media/370x300/4.jpg"></a></div>
              <div class="box-1-content">
                <div class="box-1-text">
                  <p> Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                </div>
                <a class="btn btn-lg btn-read-more " href="/">READ MORE</a> </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12  col-md-4 ">
            <div class="box-1"> <a class="box-1-title" href="#">RENOVATION </a>
              <div class="media"> <a href="#"><img width="369" height="219" alt="alt" src="media/370x300/5.jpg"></a></div>
              <div class="box-1-content">
                <div class="box-1-text">
                  <p> Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                </div>
                <a class="btn btn-lg btn-read-more " href="/">READ MORE</a> </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 ">
            <section  role="main">
              <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-4 ">
                  <div class="box-1"> <a class="box-1-title" href="#">CONSULTATION </a>
                    <div class="media"> <a href="#"><img width="369" height="219" alt="alt" src="media/370x300/6.jpg"></a></div>
                    <div class="box-1-content">
                      <div class="box-1-text">
                        <p> Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                      </div>
                      <a class="btn btn-lg btn-read-more " href="/">READ MORE</a> </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12  col-md-4 ">
                  <div class="box-1"> <a class="box-1-title" href="#">GREEN BUILDINGS </a>
                    <div class="media"> <a href="#"><img width="369" height="219" alt="alt" src="media/370x300/7.jpg"></a></div>
                    <div class="box-1-content">
                      <div class="box-1-text">
                        <p> Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                      </div>
                      <a class="btn btn-lg btn-read-more " href="/">READ MORE</a> </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12  col-md-4 ">
                  <div class="box-1"> <a class="box-1-title" href="#">HOME RENOVATION </a>
                    <div class="media"> <a href="#"><img width="369" height="219" alt="alt" src="media/370x300/8.jpg"></a></div>
                    <div class="box-1-content">
                      <div class="box-1-text">
                        <p> Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                      </div>
                      <a class="btn btn-lg btn-read-more " href="/">READ MORE</a> </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
 <div class="pre-footer-wrap">
    <div class="pre-footer">
      <div class="container">
        <div class="row"> <span class="btn-location-open"> Locate Us On The Map <i class="icon-arrow-down"></i></span> </div>
      </div>
    </div>
    <div class="pre-footer-content"> <a href="https://goo.gl/maps/QYXKkaJEzzH2" target="_blank"> <img src="media/map.jpg" alt="map"/> </a></div>
  </div>
<?php include('footer.php'); ?>

<!--HOME SLIDER--> 
<script src="plugins/iview/js/iview.js"></script> 

<!-- SCRIPTS --> 
<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script> 
<script src="js/waypoints.min.js"></script> 
<script src="plugins/bxslider/jquery.bxslider.min.js"></script> 
<script src="plugins/magnific/jquery.magnific-popup.js"></script> 
<script src="plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="js/classie.js"></script> 
<!--THEME--> 
<script src="js/cssua.min.js"></script> 
<script src="js/custom.js"></script>
</body>
</html>
